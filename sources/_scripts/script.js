// Inserindo os teclas
let append = (el,ctx,clas) =>{
        let out = document.querySelector(el),
            elem = document.createElement('div');

        elem.innerText = ctx;
        elem.className = clas;

        out.append(elem);
    },
    valDisplay = (val=false) => {
        let elem = document.querySelector('textarea');
        return val? elem.value = val : elem.value;
    },
    n = 9,
    complementos = ['.','00',0],
    formulas = ['MC','MR','M-','M+','MU','GT'],
    formulas2 = ['▸','±'],
    operadores = ['√','%','÷','X'],
    operadores2 = ['=','-'],
    ativadores = ['CE','ON'],
    ativadores2 = ['OFF','AC'],
    insertNum = e => append('.num',e,'bts bnum black'),
    insertForm = e => append('.form',e,'bts bform gray'),
    insertFor2 = e => append('.for2',e,'bts bform gray'),
    insertOper = e => append('.oper',e,'bts bnum black'),
    insertOpe2 = e => append('.ope2',e,'bts bnum2 gray'),
    insertAct = e => append('.acti',e,'bts bact gray'),
    insertAct2 = e => append('.act2',e,'bts bact2 red');

for(;n>0;) insertNum(n--);

complementos.map(insertNum);
formulas.map(insertForm);
formulas2.map(insertFor2);
operadores.map(insertOper);
operadores2.map(insertOpe2);
ativadores2.map(insertAct);
ativadores.map(insertAct2);

// Ações e efeitos
function correctOperador(o) {
    let operador = ['÷','X'],
        transOpe = ['/','*'];

    if(operador.includes(o)) return transOpe[operador.indexOf(o)];
    else return o;
}

function actionClick(t){
    let value = t.target.innerText,
        operador = ['+','-','÷','X'],
        lastValue = valDisplay(),
        calcScreen = [...lastValue].map(correctOperador).join('');

    if(Number.isInteger(+value) || operador.includes(value)) valDisplay(lastValue+''+value);
    else if(value=='=') valDisplay(eval(calcScreen));
    else if(['OFF','CE'].includes(value)) valDisplay(' ');
    else if(value=='AC') valDisplay(lastValue.slice(0,-1));
    else if(value=='.' && lastValue.slice(-1)!='.') valDisplay(lastValue+'.');
    else if(value=='√') valDisplay(Math.sqrt(eval(lastValue),2));
    else if(value=='M-') valDisplay(lastValue+'-');
    else if(value=='M+') valDisplay(lastValue+'+');
    // else if(value=='%') $('textarea').val(lastValue+'+');
}

document.querySelectorAll('.bts').forEach(e=>{
    e.onclick = actionClick;
});